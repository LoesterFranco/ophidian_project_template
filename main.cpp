/*
 * Copyright 2017 Ophidian
   Licensed to the Apache Software Foundation (ASF) under one
   or more contributor license agreements.  See the NOTICE file
   distributed with this work for additional information
   regarding copyright ownership.  The ASF licenses this file
   to you under the Apache License, Version 2.0 (the
   "License"); you may not use this file except in compliance
   with the License.  You may obtain a copy of the License at
   http://www.apache.org/licenses/LICENSE-2.0
   Unless required by applicable law or agreed to in writing,
   software distributed under the License is distributed on an
   "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
   KIND, either express or implied.  See the License for the
   specific language governing permissions and limitations
   under the License.
 */

#include <iostream>
#include <string>

#include <clara.hpp>

#include <ophidian/parser/Def.h>
#include <ophidian/parser/Lef.h>
#include <ophidian/parser/Verilog.h>

#include <ophidian/design/DesignFactory.h>

int main(int argc, char** argv)
{
    using std::cout;
    using std::cerr;
    using std::endl;
    using std::string;

    string verilog_file;
    string def_file;
    string lef_file;

    bool show_help = false;
    auto cli = clara::detail::Help(show_help)
             | clara::detail::Opt(verilog_file, "verilog")["-v"]["--verilog"]("Verilog File.").required()
             | clara::detail::Opt(def_file, "def")["-d"]["--def"]("Def File.").required()
             | clara::detail::Opt(lef_file, "lef")["-l"]["--lef"]("Lef File.").required();

    auto result = cli.parse(clara::Args(argc,argv));

    if(!result) {
        cerr << "Error in command line: " << result.errorMessage() << endl;
        cout << cli << endl;
        exit(1);
    }
    if(show_help) {
        cout << cli << endl;
        exit(1);
    }

    auto def = ophidian::parser::Def{def_file};
    auto lef = ophidian::parser::Lef{lef_file};
    auto verilog = ophidian::parser::Verilog{verilog_file};

    ophidian::design::Design design{};

    ophidian::design::factory::make_design(design, def, lef, verilog);

    const auto & standard_cells = design.standard_cells();
    const auto & netlist = design.netlist();

    for(const auto& cell_it : standard_cells.range_cell())
    {
        cout << standard_cells.name(cell_it) << endl;
        for(const auto& pin: standard_cells.pins(cell_it))
        {
            cout << static_cast<int>(standard_cells.direction(pin)) << " " << standard_cells.name(pin) << " ";
        }
        cout << endl;
    }

    return 0;
}
